<?php

namespace Ox3a\Rss\Parser;

/**
 *
 * @author zaharov-a <a3axappob@gmail.com>
 * @date 16.03.2017
 * @copyright
 * @property string $title
 * @property string $link
 * @property string $description
 * @property string $author
 * @property string $category
 * @property string $comments
 * @property string $enclosure
 * @property string $guid
 * @property string $pubDate
 * @property string $source
 */
class Item {

    /**
     *
     * @var string[] 
     */
    protected $_properties = [
        'title'       => null,
        'link'        => null,
        'description' => null,
        'author'      => null,
        'category'    => null,
        'comments'    => null,
        'enclosure'   => null,
        'guid'        => null,
        'pubDate'     => null,
        'source'      => null,
    ];

    /**
     * 
     * @param \SimpleXMLElement $data
     */
    public function __construct(\SimpleXMLElement $data) {
        foreach (array_keys($this->_properties) as $field) {
            $value = $data->$field;

            $this->_properties[$field] = $value ? (string) $value : null;
        }
    }

    public function __get($name) {
        if (array_key_exists($name, $this->_properties)) {
            return $this->_properties[$name];
        }

        return null;
    }

}
