<?php

namespace Ox3a\Rss;

/**
 *
 * @author zaharov-a <a3axappob@gmail.com>
 * @date 14.03.2017
 * @copyright
 * @property Parser\Item[] $items
 * @property string $title
 * @property string $link
 * @property string $description
 * @property string $language
 * @property string $lastBuildDate
 * @property string $copyright
 * @property string $managingEditor
 * @property string $webMaster
 * @property string $pubDate
 * @property string $lastBuildDate
 * @property string $category
 * @property string $generator
 * @property string $docs
 * @property string $cloud
 * @property string $ttl
 * @property string $image
 * @property string $rating
 * @property string $textInput
 * @property string $skipHours
 * @property string $skipDays
 */
class Parser {

    /**
     * 
     * @type SimpleXMLElement
     */
    protected $_xml;

    /**
     *
     * @var string[] 
     */
    protected $_properties = [
        'title'          => null,
        'link'           => null,
        'description'    => null,
        'language'       => null,
        'lastBuildDate'  => null,
        'copyright'      => null,
        'managingEditor' => null,
        'webMaster'      => null,
        'pubDate'        => null,
        'lastBuildDate'  => null,
        'category'       => null,
        'generator'      => null,
        'docs'           => null,
        'cloud'          => null,
        'ttl'            => null,
        'image'          => null,
        'rating'         => null,
        'textInput'      => null,
        'skipHours'      => null,
        'skipDays'       => null,
    ];

    /**
     *
     * @var \Ox3a\Rss\Parser_Item[]
     */
    protected $_items;

    public function __get($name) {
        if ($name === 'items') {
            return $this->_items;
        }

        if (array_key_exists($name, $this->_properties)) {
            return $this->_properties[$name];
        }

        return null;
    }

    public static function get($data) {
        $object = new self();
        $object->load($data);

        return $object;
    }

    public function load($data) {
        $this->_xml = simplexml_load_string($data);

        if (false === $this->_xml) {
            throw new \Exception('Invalid XML', 500);
        }

        if (!$this->_xml->channel) {
            throw new \Exception('Invalid RSS', 500);
        }

        $this->_parse();
    }

    public function getXml() {
        return $this->_xml;
    }

    protected function _parse() {
        $this->_items = [];

        foreach (array_keys($this->_properties) as $field) {
            $value = $this->_xml->channel->$field;

            $this->_properties[$field] = $value ? (string) $value : null;
        }

        foreach ($this->_xml->channel->item as $itemXml) {
            $this->_items[] = new Parser\Item($itemXml);
        }
    }

}
